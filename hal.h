/* --COPYRIGHT--,BSD
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
/*
 * ======== hal.h ========
 *
 * Device and board specific pins need to be configured here
 *
 */

/*----------------------------------------------------------------------------
 * The following function names are deprecated.  These were updated to new
 * names to follow OneMCU naming convention.
 +---------------------------------------------------------------------------*/

#ifndef DEPRECATED
#define initPorts USBHAL_initPorts
#define initClocks USBHAL_initClocks
#endif

#define SLAVE_ADDRESS 0x04

/*
 * enum i2c_cmd: Commands to communicate with cc1352 over i2c
 *
 * @BUZZER_SHORT: Short buzz
 * @BUZZER_LONG: Long buzz
 * @LED1_ON: Start LED1
 * @LED1_OFF: Stop LED1
 * @LED2_ON: Start LED2
 * @LED2_OFF: Stop LED2
 * @LED3_ON: Start LED3
 * @LED3_OFF: Stop LED3
 */
enum i2c_cmd {
  BUZZER_OFF = 0,
  BUZZER_ON,
  LED1_ON,
  LED1_OFF,
  LED2_ON,
  LED2_OFF,
  LED3_ON,
  LED3_OFF,
};

void USBHAL_initPorts(void);
void USBHAL_initClocks(uint32_t mclkFreq);
// Released_Version_5_20_06_02

//*****************************************************************************
//
//! Control the external BOOT line.
//!
//! \param active is bool, TRUE makes BOOT active.
//!
//!  Mimic open-drain operation, active low
//!  Drive low for boot mode, Input for normal
//
//*****************************************************************************
void hal_ext_boot(uint8_t active);

//*****************************************************************************
//
//! Read the external BOOT line.
//!
//! \return TRUE when high, FALSE when low
//
//*****************************************************************************
uint8_t hal_ext_boot_read(void);

//*****************************************************************************
//
//! Control the external RESET line.
//!
//! \param active is bool, TRUE in reset.
//!
//!  Mimic open-drain operation, active low
//!  Drive low for reset, Input for normal
//
//*****************************************************************************
void hal_ext_reset(uint8_t active);

//*****************************************************************************
//
//! Control the UART connection.
//!
//! \param active is bool, TRUE to connect UART to external chip.
//!
//!  Active high
//!  Drive high for UART, low to disconnect
//
//*****************************************************************************
void hal_ext_uart(uint8_t active);

void hal_ext_led(int led, int state);

void hal_ext_i2c(void);
